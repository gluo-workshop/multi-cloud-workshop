# Lab 00 - Prerequisites #

## 1. Join Slack channel ##

Join the Slack workspace through:

* URL: https://join.slack.com/t/gluomulticlou-dl72588/shared_invite/zt-2yp40nuuo-Hf6wusEvFUzOsAGIeeHjig

## 2. AWS Console login ##

Verify that you can succesfully login to AWS:

* URL: https://gluo-workshop.signin.aws.amazon.com/console
* USERNAME: `<will be provided during workshop>`
* PASSWORD: `<will be provided during workshop>`

## 5. Create GitLab account ##

If you do not already have a GitHub account, create one for free:

* URL: https://gitlab.com/users/sign_up

## 6. GitLab request access ##

We will be using GitLab CI to create our pipeline:

* URL: To start this workshop you need access to our pre-created GitLab group. When you are assigned a group number, go to the following url and ***replace the X*** with your group number. (https://gitlab.com/gluo-workshops/multicloud-workshop/group-X)
- Click on `Request access`. An instructor will then accept your request.
![](../images/requestGitLabAccess.png)

## 7. Create Docker Hub account ##

If you do not already have a Docker Hub account, create one for free:

* URL: https://hub.docker.com/signup

## 8. Linux VM login ##
---

**!!IMPORTANT!!!**

- SSH private key will be provided during the workshop.
- If using Putty, you must have the latest version installed to prevent any problems.

---

### **On Windows** ###
If you are on windows, you need a `workshop_key.ppk`.

Once you have the `workshop_key.ppk` file you can follow the steps below to connect to you management vm.
1. Open Putty.
1. Under `Connection->SSH->Auth`
    1. Click `Browse`.
    1. Choose the .ppk file: `workshop_key.ppk`.
1. Under `Session`
    1. Fill in `ubuntu@X.mgmt.workshop.aws.gluo.cloud`.
    1. Click `Open`.
1. Accept the fingerprint (if needed).
1. You're now logged in as the `ubuntu` user!
1. Please ***verify*** that the prompt's hostname number reflects `your own ID`! For example, if your ID is `1` the prompt should say `ubuntu@management-server1:~$`. If this is not the case you have logged in to the wrong management instance.

### **On Linux or MacOS** ###

1. Open your Terminal.
1. Make sure the key file has no permissions on anyone but the owner.
    ```
    chmod 400 workshop_key.pem
    ```
1. Log in to the server with the private key.
    ```
    ssh -i workshop_key.pem ubuntu@X.mgmt.workshop.aws.gluo.cloud
    ```
1. Accept the fingerprint (if needed).
1. You're now logged in as the `ubuntu` user!
1. Please ***verify*** that the prompt's hostname number reflects `your own ID`! For example, if your ID is `1` the prompt should say `ubuntu@management-server1:~$`. If this is not the case you have logged in to the wrong management instance.

## 9. Setup SSH in Gitlab ##
To be able to clone, push and pull from Gitlab repos we have 2 options. Using Gitlab credentials or setting up SSH in Gitlab. In this workshop we will use the SSH approach.
First we need to create our SSH key. **SSH into your management EC2 instance** and run the following command:
```
ssh-keygen -t rsa -b 2048
```
`Enter`
`Enter`
`Enter`

```
cat ~/.ssh/id_rsa.pub
```

Copy the contents of your public key file.


Next, go to https://gitlab.com/-/user_settings/ssh_keys
Click on `Add new key` in the top right. And past the contents of your public key in the key field.
Then click on `Add key`


**Verify that you can connect**
```
ssh -T git@gitlab.com
```

If this is the first time you connect, you should verify the authenticity of the GitLab host. If you see a message like:
```
The authenticity of host 'gitlab.com (35.231.145.151)' can't be established.
ECDSA key fingerprint is SHA256:HbW3g8zUjNSksFbqTiUWPWg2Bq1x8xdGUrliXFzSnUw.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'gitlab.com' (ECDSA) to the list of known hosts.
```
Type `yes` and press **Enter**.

Run the `ssh -T git@gitlab.com` command again. You should receive a 'Welcome to GitLab, @username!' message.

## RUN CHECKSCORE SCRIPT ON YOUR MANAGEMENT VM

Run the checkscore command.
- `sudo checkscore 0`
When you get the following message, you need to fill in the number you were assinged at the beginning of the workshop: 'Please fill in your group number here.'

---

Go to the next lab: [Lab 01 - Docker](../lab01_docker)
