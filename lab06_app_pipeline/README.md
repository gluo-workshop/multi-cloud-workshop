# Lab 06 - Application Pipeline #

## 1. Create CI/CD variables

Go to your previously assigned gitlab group and click on your **multi-cloud-app** repository (https://gitlab.com/gluo-workshops/multicloud-workshop/group-X/multi-cloud-app).

Before creating the pipeline to setup the application, we will need to create two CI/CD variables. These are variables stored in the project settings and can be referenced in the pipeline configuration. The variables needed are:

- `DOCKER_REGISTRY_USER` : the username of your personal Docker Hub account.
- `DOCKER_REGISTRY_PASSWORD` : the password of your personal Docker Hub account.

### Instructions:

1. In GitLab, hover over **Settings** in the left sidebar and click on **CI/CD.**  

​	![](../images/gitlabCI_CDSettings.png)	

&nbsp;  

2. Then click on the **Expand** button (top right corner in variables section).

![](../images/gitlabOpenVariablesTab.png) 

&nbsp;  

3. Next, under **Variables** click on **Add variable** and create a `DOCKER_REGISTRY_USER` variable with your personal Docker Hub username as the value. Select the **Mask variable** checkbox and unselect the other checkboxes. Then click on the **Add variable** button.

​	![](../images/gitlabAddVariable.png) 

&nbsp;  

4. Do the same for the `DOCKER_REGISTRY_PASSWORD` variable.

**Note:** masking variables will hide the values in the output of the pipeline. To mask a variable it is required for a value to match certain criteria such as minimum length, to ensure the safety of these sensitive variables.  

&nbsp;   

## 2. Create your pipeline ##

We will now create a new pipeline to build and deploy our application to each cloud provider. 

Go back to the **multi-cloud-app** repository homepage. In the left menu click on `CI/CD -> Editor`  .

![](../images/createPipeline.png) 

&nbsp;  

Next, click on the **Configure pipeline** button to initialize a new pipeline.
![](../images/initializePipeline.png) 

This will take you to the pipeline editor page. Clear all code in this **.gitlab-ci.yml** file so you have an empty editor screen and stay on the editor page.
![](../images/pipelineStartState.png)

&nbsp;  

## 3. Edit your build pipeline ##

Copy & paste the following code snippets in the same order as they are shown.
We start again with with defining some global variables. The `DOCKER_REGISTRY_URL` variable defines the URL used to connect to Docker Hub. 

```yaml
variables: 
  DOCKER_REGISTRY: docker.io
  DOCKER_REGISTRY_URL: "https://index.docker.io/v1/"
  IMAGE_NAME: multi-cloud-app
  IMAGE_TAG: latest
```
In the next step we add the default image configuration which in this case is used by all underlying pipeline jobs.
```yaml
default:
  image: alpine:latest
```

Then we define the pipeline stages.

```yaml
stages:
  - build_app
  - deploy_app
```

In the first stage we use `Kaniko` from Google to build our application and to push this image to the Docker Hub container registry.

---
**NOTE**:

Kaniko is a tool to build container images from a Dockerfile, inside a container or Kubernetes cluster.

Kaniko doesn't depend on a Docker daemon and executes each command within a Dockerfile completely in userspace. This enables building container images in environments that can't easily or securely run a Docker daemon, such as a standard Kubernetes cluster.

---

```yaml
build_&_push_with_kaniko:
  stage: build_app
  image:
    name: gcr.io/kaniko-project/executor:v1.9.0-debug
    entrypoint: [""]
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"${DOCKER_REGISTRY_URL}\":{\"auth\":\"$(printf "%s:%s" "${DOCKER_REGISTRY_USER}" "${DOCKER_REGISTRY_PASSWORD}" | base64 | tr -d '\n')\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
      --destination "${DOCKER_REGISTRY}/${DOCKER_REGISTRY_USER}/${IMAGE_NAME}:${IMAGE_TAG}"
```

In the next pipeline job first run a before script that uses ssh-agent and the `SSH_PRIVATE_KEY` variable from GitLab to scan the public key of the remote hosts and adds them to the known_hosts file of the build environment. Then an ssh command is run to remove any containers already running to prevent the port already in use Docker error. Finaly we start a new docker container on the AWS vm with the image created in the previous stage.

```yaml
deploy_to_aws:
  stage:  deploy_app
  before_script:
    - mkdir -p ~/.ssh
    - chmod 400 ~/.ssh
    # Install ssh-agent if not already installed.
    - 'ssh-agent > /dev/null || ( apk add --update openssh )'
    # Run ssh-agent (inside the build environment)
    - eval $(ssh-agent -s)
    # Add the SSH key stored in SSH_PRIVATE_KEY variable to the agent store
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    # Scan the remote host public key and if approved add the host to the known_hosts file
    - ssh-keyscan $VM_IPADDRESS_AWS >> ~/.ssh/known_hosts
  script:
    - ssh ubuntu@$VM_IPADDRESS_AWS "docker ps -aq | xargs docker stop | xargs docker rm || true" # remove container if it exists to prevent port already in use
    - ssh ubuntu@$VM_IPADDRESS_AWS "docker run --pull always -d -p 80:80 ${DOCKER_REGISTRY_USER}/${IMAGE_NAME}:${IMAGE_TAG}"
```

Then we do the same for the Azure vm ...
```yaml
deploy_to_azure:
  stage:  deploy_app
  before_script:
    - mkdir -p ~/.ssh
    - chmod 400 ~/.ssh
    # Install ssh-agent if not already installed.
    - 'ssh-agent > /dev/null || ( apk add --update openssh )'
    # Run ssh-agent (inside the build environment)
    - eval $(ssh-agent -s)
    # Add the SSH key stored in SSH_PRIVATE_KEY variable to the agent store
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    # Scan the remote host public key and if approved add the host to the known_hosts file
    - ssh-keyscan $VM_IPADDRESS_AZURE >> ~/.ssh/known_hosts
  script:
    - ssh ubuntu@$VM_IPADDRESS_AZURE "docker ps -aq | xargs docker stop | xargs docker rm || true" # remove container if it exists to prevent port already in use 
    - ssh ubuntu@$VM_IPADDRESS_AZURE "docker run --pull always -d -p 80:80 ${DOCKER_REGISTRY_USER}/${IMAGE_NAME}:${IMAGE_TAG}"
```

&nbsp;  

## DON'T FORGET TO COMMIT YOUR CHANGES!!

Committing these changes will automatically trigger the pipeline. Go to **CI/CD** in the left sidebar and click on **Pipelines** in the list to view the running pipeline.  

After the pipeline has succeeded you can visit the different webpages and should see the webapp.

- X.tf.workshop.azure.gluo.cloud
- X.tf.workshop.aws.gluo.cloud


## 4. Change application background color ##

Let's log back into our management server and change directory: 

```
cd ~/multi-cloud-app
```

Pull the changes from the repo.

```
git pull
```

Change the color in `<div class="card green">` in the index.php file from green to blue.

```php
<!-- -------------------------- -->
<!-- !!! CHANGE COLOR BELOW !!! -->

# <div class="card green">
<div class="card blue">

<!-- !!! CHANGE COLOR ABOVE !!! -->
<!-- -------------------------- -->
```

Once done editing we can push the changes to GitLab.  

```
cd ~/multi-cloud-app
```

```
git config --global user.email "YOUR_GITLAB_EMAILADDRESS"
```

```
git add .
```

```
git commit -m "Changed background color from green to blue"
```
```
git push origin main
```

Open the **Pipelines** page. You will see that a new pipeline has started after pushing the new code.

![](../images/pipelinesView.png)

After the pipeline has succeeded you can visit the different webpages and should see that the background has changed.

- X.tf.workshop.azure.gluo.cloud
- X.tf.workshop.aws.gluo.cloud

## RUN CHECKSCORE SCRIPT ON YOUR MANAGEMENT VM

After the pipeline has successfully finished, run the checkscore command.
- `sudo checkscore 6`

&nbsp;  

## 5. Destroying the infrastructure

At the end of this lab you can destroy the infrastructure by running the infrastructure pipeline and passing along a variable to trigger the destruction of all resources.  

Go to your **multi-cloud-workshop** repo in GitLab. To run the pipeline, go to **CI/CD** in the left sidebar. In the top right corner, click the **Run pipeline** button.

![image-20230102163201659](../images/gitlabRunPipeline.png)



Before running the pipeline, select the value `destroy` for the variable `ACTION`. Next, click the **Run pipeline** button and all resources will be destroyed. 

![](../images/destroyInfraPipeline.png)

---

**====> Congratulations! You have successfully finished the Gluo multi-cloud workshop!  <====**