# Lab 05 -  Infrastructure pipeline #

Go into the management instance and change directory:
`cd ~/multi-cloud-workshop/lab04_terraform`

In the previous lab the groupID configured in variables.tf in the 2 folders (aws, azure) should have already been changed to your group number. Now go back to the lab04 directory, and this time make a change in the `main.tf` file by altering the value of the terraform backend from `local` to `http` in the 2 folders.

```vim
provider "aws" {
}

terraform {
  backend "local" { // <==== Change this to "http" to run in the GitLab pipeline
  }
}
```

Once done editing we can push this back to GitLab.  
```
cd ~/multi-cloud-workshop
```

```
git config --global user.email "YOUR_GITLAB_EMAILADRESS"
```

```
git add .
```

```
git commit -m "Changed groupID variables and terraform backend"
```
```
git push origin main
```

## 2. Create your pipeline ##

Go to your previously assigned gitlab group and click on your repository. 
* https://gitlab.com/gluo-workshops/multicloud-workshop/group-X/multi-cloud-workshop

Then in the left menu click on Build -> Pipeline editor
![](../images/createPipeline.png)

Next, click on configure pipeline to initialize a new pipeline.
![](../images/initializePipeline.png)

This will take you to the pipeline editor page. Clear all code in this *gitlab-ci.yml* file so you have an empty editor screen and stay on the editor page.
![](../images/pipelineStartState.png)

## 3. Edit your build pipeline ##
Copy & paste the following code snippets in the same order as they are shown.
We start with defining some variables which tell Terraform where to store the state file.

```yaml
variables:
  TF_ADDRESS: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${TF_STATE}
  ACTION:
    description: "Specifies whether to create, destroy the infra"
    value: "apply"
    options:
      - "apply"
      - "destroy"
```

---
**NOTE**: Keep in mind that the following cloud authentication variables are already set as group variables in the GitLab group. These variables are then used to connect to the different cloud providers APIs:  
* `AWS_ACCESS_KEY_ID`  
* `AWS_SECRET_ACCESS_KEY`  
* `AWS_DEFAULT_REGION`  
* `ARM_CLIENT_ID`  
* `ARM_SUBSCRIPTION_ID`  
* `ARM_TENANT_ID`  
* `ARM_CLIENT_SECRET`  
* `SSH_PRIVATE_KEY`  
* `SSH_PUBLIC_KEY`  
* `VM_IPADDRESS_AWS`  
* `VM_IPADDRESS_AZURE`  
* `VM_IPADDRESS_GOOGLE`  
---

In the next step we add the default image configuration which in this case is used by all underlying pipeline jobs.
```yaml
default:
  image:
    name: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
    entrypoint:
      - /usr/bin/env
      - "PATH=/usr/local/bin:usr/local/sbin:/usr/sbin:/usr/bin:/bin:/sbin"
```

Next we create an anchor, which comes in handy because we don't want to write the same code (in this case a before_script) several times in this pipeline configuration. The commands in this before_script take the `SSH_PUBLIC_KEY` variable from GitLab and create a public key file named workshop_key.pub in the `~/.ssh` directory.  

```yaml
.before_script_template1: &setup_ssh_and_check_http_backend
  before_script:
    - mkdir ~/.ssh/
    - touch ~/.ssh/workshop_key.pub
    - echo ${SSH_PUBLIC_KEY} > ~/.ssh/workshop_key.pub
    - cd ${TF_ROOT}
    - echo $VALIDATE_HTTP_BACKEND | base64 -d > validate_http_backend.sh
    - chmod +x validate_http_backend.sh
    - sh ./validate_http_backend.sh
```

Then we define the pipeline stages. In this case there is only 2 stages, the apply_infra and destroy_infra stage. The destroy_infra stage will be implemented so that it only runs when a variable is set when starting a pipeline.
```yaml
stages:
  - apply_infra
  - destroy_infra
```

The next snippet runs the Terraform code to deploy on AWS and stores the Terraform state on Gitlab in a statefile called `aws`. This is set using the `TF_STATE` variable.  
The variable `TF_ROOT` points to the aws subdirectory so Terraform knows where the config files for this environment are stored. The before_script we created before as an anchor is now reused (merged) in this stage and referenced with `<<: *setup_ssh_and_check_http_backend`.
```yaml
terraform_apply_aws:
  stage:  apply_infra
  variables:
    TF_STATE: aws
    TF_ROOT: ${CI_PROJECT_DIR}/lab04_terraform/aws
  <<: *setup_ssh_and_check_http_backend
  script:
    - gitlab-terraform init 
    - gitlab-terraform plan -var 'lab=5'
    - gitlab-terraform apply --auto-approve
  rules:
    - if: $ACTION == "apply"
```

Do the same for the Azure infrastructure ...
```yaml
terraform_apply_azure:
  stage:  apply_infra
  variables:
    TF_STATE: azure
    TF_ROOT: ${CI_PROJECT_DIR}/lab04_terraform/azure
  <<: *setup_ssh_and_check_http_backend
  script:
    - gitlab-terraform init
    - gitlab-terraform plan -var 'lab=5'
    - gitlab-terraform apply --auto-approve
  rules:
    - if: $ACTION == "apply"
```

**Do not commit your changes yet.** We need to add another stage, the destroy_infra stage.

## 4. Create a release stage to destroy our setup  ##
Without leaving the edit page, you need to add the following code.
To create a destroy infrastructure stage add the following code snippet . This code is identical to the apply_infra stage except the terraform apply is switched out for a terraform destroy command. The terraform destroy command uses the state file stored on GitLab to destroy the setup.
```yaml
terraform_destroy_aws:
  stage: destroy_infra
  variables:
    TF_STATE: aws
    TF_ROOT: ${CI_PROJECT_DIR}/lab04_terraform/aws
  <<: *setup_ssh_and_check_http_backend
  script:
    - gitlab-terraform init 
    - gitlab-terraform destroy --auto-approve
  rules:
    - if: $ACTION != "apply"

terraform_destroy_azure:
  stage: destroy_infra
  variables:
    TF_STATE: azure
    TF_ROOT: ${CI_PROJECT_DIR}/lab04_terraform/azure
  <<: *setup_ssh_and_check_http_backend
  script:
    - gitlab-terraform init 
    - gitlab-terraform destroy --auto-approve
  rules:
    - if: $ACTION != "apply"
```

### **DON'T FORGET TO COMMIT YOUR CHANGES!!**

Then click on pipelines in the left sidebar to view the running pipeline
`DO NOT DESTROY THE INFRASTRUCTURE AT THIS POINT.` We still need it for our next lab.

## RUN CHECKSCORE SCRIPT ON YOUR MANAGEMENT VM

After the pipeline has succeeded you can visit the different nginx webpages and should see that the background has changed.

- X.tf.workshop.azure.gluo.cloud
- X.tf.workshop.aws.gluo.cloud

After the pipeline has successfully finished, run the checkscore command.
- `sudo checkscore 5`

---

Go to the next lab: [Lab 06 - Application pipeline](../lab06_app_pipeline)