# Lab 04 - Terraform #

In this lab we will be using [Terraform](https://www.terraform.io/).

Terraform is an Infrastructure-as-Code tool for building, changing, and versioning infrastructure safely and efficiently. Terraform can manage existing and popular service providers as well as custom in-house solutions.

Terraform uses configuration files which describe the components needed to run a single application or your entire datacenter. Terraform generates an execution plan describing what it will do to reach the desired state, and then executes it to build the described infrastructure. As the configuration changes, Terraform is able to determine what changed and create incremental execution plans which can be applied.

The infrastructure that Terraform can manage includes low-level components such as compute instances, storage, and networking, as well as high-level components such as DNS entries, SaaS features, etc.

In this lab we will be using Terraform to allow us to use the same tool/language to deploy our application to all 3 public cloud providers. Terraform is already preinstalled on the management servers.

**In this lab we will be working from our management server again, so SSH into X.mgmt.gluo.cloud**.

## 1. AWS configuration ##

Before we can deploy our infrastructure into AWS using Terraform we need to make some small changes to the Terraform code.  More specifically we need to edit the `variables.tf` file and specify your group ID. To do so use either `vi`, `vim` or `nano`.  For example:

```yaml
vim ~/multi-cloud-workshop/lab04_terraform/aws/variables.tf

---

# replace the X with your group ID
variable "groupID" {
  default = "X"   // <========= make change here
}

# ---- DO NOT EDIT anything below this line -----------------------------

variable "lab" {
  default = "4"
}

variable "dns_zone_id" {
  default = "Z092701712SREG91O80DN"
}

variable "aws_machine_type" {
  default = "t2.micro"
}

variable "aws_region" {
  default = "eu-west-1"
}

variable "aws_ami" {
  default = "ami-01dd271720c1ba44f" # Ubuntu Server 22.04 LTS (HVM), SSD Volume Type
}

```

If you have successfully edited and saved the `variables.tf` file you can change into the directory that contains the Terraform code for AWS:

```
cd ~/multi-cloud-workshop/lab04_terraform/aws/
```

Have a look at the content of the Terraform file `main.tf`:

```yaml
cat main.tf

---
provider "aws" {
}

terraform {
  backend "local" {
  }
}

resource "aws_instance" "ec2-instance" {
  ami             = "${var.aws_ami}"
  instance_type   = "${var.aws_machine_type}"
  key_name        = "${aws_key_pair.workshop_key.key_name}"
  
  security_groups = [
    "${aws_security_group.allow_ssh.name}",
    "${aws_security_group.allow_http.name}",
    "${aws_security_group.allow_outbound.name}"
  ]

  user_data       = templatefile("cloud-init.yaml",{group_id = "${var.groupID}"})

  tags = {
    Name = "terraform-instance-aws-group${var.groupID}"
    group = "${var.groupID}"
    lab = "${var.lab}"
  }
}

resource "aws_security_group" "allow_ssh" {
  name        = "allow-ssh-group${var.groupID}"
  description = "Allow SSH inbound traffic"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "allow_http" {
 name        = "allow-http-group${var.groupID}"
 description = "Allow HTTP inbound traffic"
 ingress {
   from_port   = 80
   to_port     = 80
   protocol    = "tcp"
   cidr_blocks = ["0.0.0.0/0"]
 }
}

resource "aws_security_group" "allow_outbound" {
  name        = "allow-all-outbound-group${var.groupID}"
  description = "Allow all outbound traffic"

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_key_pair" "workshop_key" {
  key_name   = "workshop-key-group${var.groupID}"
  public_key = "${file("~/.ssh/workshop_key.pub")}"
}

resource "aws_route53_record" "gluo-cloud" {
  zone_id = var.dns_zone_id
  name    = "${var.groupID}.tf.workshop.aws.gluo.cloud."
  type    = "CNAME"
  ttl     = "60"
  records = ["${aws_instance.ec2-instance.public_dns}"]
}
```  

There is also the presence of a `cloud-init.yaml` file, which contains the Docker installation and the Docker command to run the multi-cloud application. This file will be referenced in `main.tf` as user_data in the EC2 instance resource. This way, when the EC2 created by Terraform boots, this file will run at startup. 

Now we need to initialize our working directory for AWS by using the `terraform init` command.

The terraform init command is used to initialize a working directory containing Terraform configuration files. This is the first command that should be run after writing a new Terraform configuration or cloning an existing one from version control. It is safe to run this command multiple times.

```yaml
terraform init

---

Initializing the backend...

Successfully configured the backend "local"! Terraform will automatically
use this backend unless the backend configuration changes.

Initializing provider plugins...
- Finding latest version of hashicorp/aws...
- Installing hashicorp/aws v4.34.0...
- Installed hashicorp/aws v4.34.0 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary
```

Now we can run the `terraform plan` command.

The terraform plan command is used to create an execution plan. Terraform performs a refresh, unless explicitly disabled, and then determines what actions are necessary to achieve the desired state specified in the configuration files.

This command is a convenient way to check whether the execution plan for a set of changes matches your expectations without making any changes to real resources or to the state. For example, terraform plan might be run before committing a change to version control, to create confidence that it will behave as expected.

```yaml
terraform plan

---

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_instance.ec2-instance will be created
  + resource "aws_instance" "ec2-instance" {
      + ami                                  = "ami-096800910c1b781ba"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = (known after apply)
      + availability_zone                    = (known after apply)
      + cpu_core_count                       = (known after apply)
      + cpu_threads_per_core                 = (known after apply)
      + disable_api_stop                     = (known after apply)
      + disable_api_termination              = (known after apply)
      + ebs_optimized                        = (known after apply)
      + get_password_data                    = false
      + host_id                              = (known after apply)
      + host_resource_group_arn              = (known after apply)
      + id                                   = (known after apply)
      + instance_initiated_shutdown_behavior = (known after apply)
      + instance_state                       = (known after apply)
      + instance_type                        = "t2.micro"
      + ipv6_address_count                   = (known after apply)
      + ipv6_addresses                       = (known after apply)
      + key_name                             = "workshop-key-group5"
      + monitoring                           = (known after apply)
      + outpost_arn                          = (known after apply)
      + password_data                        = (known after apply)
      + placement_group                      = (known after apply)
      + placement_partition_number           = (known after apply)
      + primary_network_interface_id         = (known after apply)
      + private_dns                          = (known after apply)
      + private_ip                           = (known after apply)
      + public_dns                           = (known after apply)
      + public_ip                            = (known after apply)
      + secondary_private_ips                = (known after apply)
      + security_groups                      = [
          + "allow-all-outbound-group5",
          + "allow-http-group5",
          + "allow-ssh-group5",
        ]
      + source_dest_check                    = true
      + subnet_id                            = (known after apply)
      + tags                                 = {
          + "Name" = "terraform-instance-aws-group5"
        }
      + tags_all                             = {
          + "Name" = "terraform-instance-aws-group5"
        }
      + tenancy                              = (known after apply)
      + user_data                            = "145950b31144ee4242c2b50cea77c2a911a7911f"
      + user_data_base64                     = (known after apply)
      + user_data_replace_on_change          = false
      + vpc_security_group_ids               = (known after apply)

      + capacity_reservation_specification {
          + capacity_reservation_preference = (known after apply)

          + capacity_reservation_target {
              + capacity_reservation_id                 = (known after apply)
              + capacity_reservation_resource_group_arn = (known after apply)
            }
        }

      + ebs_block_device {
          + delete_on_termination = (known after apply)
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + snapshot_id           = (known after apply)
          + tags                  = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }

      + enclave_options {
          + enabled = (known after apply)
        }

      + ephemeral_block_device {
          + device_name  = (known after apply)
          + no_device    = (known after apply)
          + virtual_name = (known after apply)
        }

      + maintenance_options {
          + auto_recovery = (known after apply)
        }

      + metadata_options {
          + http_endpoint               = (known after apply)
          + http_put_response_hop_limit = (known after apply)
          + http_tokens                 = (known after apply)
          + instance_metadata_tags      = (known after apply)
        }

      + network_interface {
          + delete_on_termination = (known after apply)
          + device_index          = (known after apply)
          + network_card_index    = (known after apply)
          + network_interface_id  = (known after apply)
        }

      + private_dns_name_options {
          + enable_resource_name_dns_a_record    = (known after apply)
          + enable_resource_name_dns_aaaa_record = (known after apply)
          + hostname_type                        = (known after apply)
        }

      + root_block_device {
          + delete_on_termination = (known after apply)
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + tags                  = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }
    }

  # aws_key_pair.workshop_key will be created
  + resource "aws_key_pair" "workshop_key" {
      + arn             = (known after apply)
      + fingerprint     = (known after apply)
      + id              = (known after apply)
      + key_name        = "workshop-key-group5"
      + key_name_prefix = (known after apply)
      + key_pair_id     = (known after apply)
      + key_type        = (known after apply)
      + public_key      = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCiPMnEZoPD3ZtnA2V3Zlpo+z1BdwDnztD38WqYeFqhFQyNIABuxsKc+OiZfxm3R4g+VyNWqRT4poPex/JIHq9B8ACIAZdfGWe05xHXtas4XqshXxweocK8Y2lsd2wehWsJ4gH9vVyg/JvxXAxfNEEVxzodD9MFJJNjtTsx6vH+6PhsiG3xmql7fUDEIp/tLFJ7nzKKFbV4hPLaCS5eNSxyyjkL52VvIrh5SxhebAJMaVVvjhJPrH3pELUX2hMcKOaocqJ02/WnLbki6+p+zCaL6xIMMwfajXbQmfb6FoF1X72V08/Ll/3lO7EGZxvq75rB+v3y9C9QrtYHXM3++jpV ubuntu@studentX-management"
      + tags_all        = (known after apply)
    }

  # aws_route53_record.gluo-cloud will be created
  + resource "aws_route53_record" "gluo-cloud" {
      + allow_overwrite = (known after apply)
      + fqdn            = (known after apply)
      + id              = (known after apply)
      + name            = "5.tf.aws.gluo.cloud"
      + records         = (known after apply)
      + ttl             = 60
      + type            = "CNAME"
      + zone_id         = "Z130EX75117ZMX"
    }

  # aws_security_group.allow_http will be created
  + resource "aws_security_group" "allow_http" {
      + arn                    = (known after apply)
      + description            = "Allow HTTP inbound traffic"
      + egress                 = (known after apply)
      + id                     = (known after apply)
      + ingress                = [
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = ""
              + from_port        = 80
              + ipv6_cidr_blocks = []
              + prefix_list_ids  = []
              + protocol         = "tcp"
              + security_groups  = []
              + self             = false
              + to_port          = 80
            },
        ]
      + name                   = "allow-http-group5"
      + name_prefix            = (known after apply)
      + owner_id               = (known after apply)
      + revoke_rules_on_delete = false
      + tags_all               = (known after apply)
      + vpc_id                 = (known after apply)
    }

  # aws_security_group.allow_outbound will be created
  + resource "aws_security_group" "allow_outbound" {
      + arn                    = (known after apply)
      + description            = "Allow all outbound traffic"
      + egress                 = [
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = ""
              + from_port        = 0
              + ipv6_cidr_blocks = []
              + prefix_list_ids  = []
              + protocol         = "-1"
              + security_groups  = []
              + self             = false
              + to_port          = 0
            },
        ]
      + id                     = (known after apply)
      + ingress                = (known after apply)
      + name                   = "allow-all-outbound-group5"
      + name_prefix            = (known after apply)
      + owner_id               = (known after apply)
      + revoke_rules_on_delete = false
      + tags_all               = (known after apply)
      + vpc_id                 = (known after apply)
    }

  # aws_security_group.allow_ssh will be created
  + resource "aws_security_group" "allow_ssh" {
      + arn                    = (known after apply)
      + description            = "Allow SSH inbound traffic"
      + egress                 = (known after apply)
      + id                     = (known after apply)
      + ingress                = [
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = ""
              + from_port        = 22
              + ipv6_cidr_blocks = []
              + prefix_list_ids  = []
              + protocol         = "tcp"
              + security_groups  = []
              + self             = false
              + to_port          = 22
            },
        ]
      + name                   = "allow-ssh-group5"
      + name_prefix            = (known after apply)
      + owner_id               = (known after apply)
      + revoke_rules_on_delete = false
      + tags_all               = (known after apply)
      + vpc_id                 = (known after apply)
    }

Plan: 6 to add, 0 to change, 0 to destroy.

------------------------------------------------------------------------

Note: You didn't use the -out option to save this plan, so Terraform can't guarantee to take exactly these actions if you run "terraform apply" now.
```

Pay special attention to one of the last lines, it reads:

```
Plan: 6 to add, 0 to change, 0 to destroy.
```

This gives you a good overview of what Terraform wil do (add, change and/or destroy).  If you agree with what Terraform is going to to do you can run the `terraform apply` command.

The terraform apply command is used to apply the changes required to reach the desired state of the configuration, or the pre-determined set of actions generated by a terraform plan execution plan.

> NOTE: Terraform will prompt you if it should proceed, type `yes` and hit ENTER

```yaml
terraform apply

---

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_instance.ec2-instance will be created
  + resource "aws_instance" "ec2-instance" {
      + ami                                  = "ami-096800910c1b781ba"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = (known after apply)
      + availability_zone                    = (known after apply)
      + cpu_core_count                       = (known after apply)
      + cpu_threads_per_core                 = (known after apply)
      + disable_api_stop                     = (known after apply)
      + disable_api_termination              = (known after apply)
      + ebs_optimized                        = (known after apply)
      + get_password_data                    = false
      + host_id                              = (known after apply)
      + host_resource_group_arn              = (known after apply)
      + id                                   = (known after apply)
      + instance_initiated_shutdown_behavior = (known after apply)
      + instance_state                       = (known after apply)
      + instance_type                        = "t2.micro"
      + ipv6_address_count                   = (known after apply)
      + ipv6_addresses                       = (known after apply)
      + key_name                             = "workshop-key-group5"
      + monitoring                           = (known after apply)
      + outpost_arn                          = (known after apply)
      + password_data                        = (known after apply)
      + placement_group                      = (known after apply)
      + placement_partition_number           = (known after apply)
      + primary_network_interface_id         = (known after apply)
      + private_dns                          = (known after apply)
      + private_ip                           = (known after apply)
      + public_dns                           = (known after apply)
      + public_ip                            = (known after apply)
      + secondary_private_ips                = (known after apply)
      + security_groups                      = [
          + "allow-all-outbound-group5",
          + "allow-http-group5",
          + "allow-ssh-group5",
        ]
      + source_dest_check                    = true
      + subnet_id                            = (known after apply)
      + tags                                 = {
          + "Name" = "terraform-instance-aws-group5"
        }
      + tags_all                             = {
          + "Name" = "terraform-instance-aws-group5"
        }
      + tenancy                              = (known after apply)
      + user_data                            = "145950b31144ee4242c2b50cea77c2a911a7911f"
      + user_data_base64                     = (known after apply)
      + user_data_replace_on_change          = false
      + vpc_security_group_ids               = (known after apply)

      + capacity_reservation_specification {
          + capacity_reservation_preference = (known after apply)

          + capacity_reservation_target {
              + capacity_reservation_id                 = (known after apply)
              + capacity_reservation_resource_group_arn = (known after apply)
            }
        }

      + ebs_block_device {
          + delete_on_termination = (known after apply)
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + snapshot_id           = (known after apply)
          + tags                  = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }

      + enclave_options {
          + enabled = (known after apply)
        }

      + ephemeral_block_device {
          + device_name  = (known after apply)
          + no_device    = (known after apply)
          + virtual_name = (known after apply)
        }

      + maintenance_options {
          + auto_recovery = (known after apply)
        }

      + metadata_options {
          + http_endpoint               = (known after apply)
          + http_put_response_hop_limit = (known after apply)
          + http_tokens                 = (known after apply)
          + instance_metadata_tags      = (known after apply)
        }

      + network_interface {
          + delete_on_termination = (known after apply)
          + device_index          = (known after apply)
          + network_card_index    = (known after apply)
          + network_interface_id  = (known after apply)
        }

      + private_dns_name_options {
          + enable_resource_name_dns_a_record    = (known after apply)
          + enable_resource_name_dns_aaaa_record = (known after apply)
          + hostname_type                        = (known after apply)
        }

      + root_block_device {
          + delete_on_termination = (known after apply)
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + tags                  = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }
    }

  # aws_key_pair.workshop_key will be created
  + resource "aws_key_pair" "workshop_key" {
      + arn             = (known after apply)
      + fingerprint     = (known after apply)
      + id              = (known after apply)
      + key_name        = "workshop-key-group5"
      + key_name_prefix = (known after apply)
      + key_pair_id     = (known after apply)
      + key_type        = (known after apply)
      + public_key      = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCiPMnEZoPD3ZtnA2V3Zlpo+z1BdwDnztD38WqYeFqhFQyNIABuxsKc+OiZfxm3R4g+VyNWqRT4poPex/JIHq9B8ACIAZdfGWe05xHXtas4XqshXxweocK8Y2lsd2wehWsJ4gH9vVyg/JvxXAxfNEEVxzodD9MFJJNjtTsx6vH+6PhsiG3xmql7fUDEIp/tLFJ7nzKKFbV4hPLaCS5eNSxyyjkL52VvIrh5SxhebAJMaVVvjhJPrH3pELUX2hMcKOaocqJ02/WnLbki6+p+zCaL6xIMMwfajXbQmfb6FoF1X72V08/Ll/3lO7EGZxvq75rB+v3y9C9QrtYHXM3++jpV ubuntu@studentX-management"
      + tags_all        = (known after apply)
    }

  # aws_route53_record.gluo-cloud will be created
  + resource "aws_route53_record" "gluo-cloud" {
      + allow_overwrite = (known after apply)
      + fqdn            = (known after apply)
      + id              = (known after apply)
      + name            = "5.tf.aws.gluo.cloud"
      + records         = (known after apply)
      + ttl             = 60
      + type            = "CNAME"
      + zone_id         = "Z130EX75117ZMX"
    }

  # aws_security_group.allow_http will be created
  + resource "aws_security_group" "allow_http" {
      + arn                    = (known after apply)
      + description            = "Allow HTTP inbound traffic"
      + egress                 = (known after apply)
      + id                     = (known after apply)
      + ingress                = [
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = ""
              + from_port        = 80
              + ipv6_cidr_blocks = []
              + prefix_list_ids  = []
              + protocol         = "tcp"
              + security_groups  = []
              + self             = false
              + to_port          = 80
            },
        ]
      + name                   = "allow-http-group5"
      + name_prefix            = (known after apply)
      + owner_id               = (known after apply)
      + revoke_rules_on_delete = false
      + tags_all               = (known after apply)
      + vpc_id                 = (known after apply)
    }

  # aws_security_group.allow_outbound will be created
  + resource "aws_security_group" "allow_outbound" {
      + arn                    = (known after apply)
      + description            = "Allow all outbound traffic"
      + egress                 = [
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = ""
              + from_port        = 0
              + ipv6_cidr_blocks = []
              + prefix_list_ids  = []
              + protocol         = "-1"
              + security_groups  = []
              + self             = false
              + to_port          = 0
            },
        ]
      + id                     = (known after apply)
      + ingress                = (known after apply)
      + name                   = "allow-all-outbound-group5"
      + name_prefix            = (known after apply)
      + owner_id               = (known after apply)
      + revoke_rules_on_delete = false
      + tags_all               = (known after apply)
      + vpc_id                 = (known after apply)
    }

  # aws_security_group.allow_ssh will be created
  + resource "aws_security_group" "allow_ssh" {
      + arn                    = (known after apply)
      + description            = "Allow SSH inbound traffic"
      + egress                 = (known after apply)
      + id                     = (known after apply)
      + ingress                = [
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = ""
              + from_port        = 22
              + ipv6_cidr_blocks = []
              + prefix_list_ids  = []
              + protocol         = "tcp"
              + security_groups  = []
              + self             = false
              + to_port          = 22
            },
        ]
      + name                   = "allow-ssh-group5"
      + name_prefix            = (known after apply)
      + owner_id               = (known after apply)
      + revoke_rules_on_delete = false
      + tags_all               = (known after apply)
      + vpc_id                 = (known after apply)
    }

Plan: 6 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

aws_security_group.allow_outbound: Creating...
aws_security_group.allow_ssh: Creating...
aws_security_group.allow_http: Creating...
aws_key_pair.workshop_key: Creating...
aws_key_pair.workshop_key: Creation complete after 0s [id=workshop-key-group5]
aws_security_group.allow_ssh: Creation complete after 2s [id=sg-0b035ef43ba4d04e0]
aws_security_group.allow_outbound: Creation complete after 2s [id=sg-0268f30beed41b6db]
aws_security_group.allow_http: Creation complete after 3s [id=sg-0124899264b848c86]
aws_instance.ec2-instance: Creating...
aws_instance.ec2-instance: Still creating... [10s elapsed]
aws_instance.ec2-instance: Still creating... [20s elapsed]
aws_instance.ec2-instance: Still creating... [30s elapsed]
aws_instance.ec2-instance: Creation complete after 31s [id=i-0dd7f2e54517351a2]
aws_route53_record.gluo-cloud: Creating...
aws_route53_record.gluo-cloud: Still creating... [10s elapsed]
aws_route53_record.gluo-cloud: Still creating... [20s elapsed]
aws_route53_record.gluo-cloud: Still creating... [30s elapsed]
aws_route53_record.gluo-cloud: Creation complete after 39s [id=Z130EX75117ZMX_5.tf.aws.gluo.cloud._CNAME]

Apply complete! Resources: 6 added, 0 changed, 0 destroyed.
```

Visit `X.tf.workshop.aws.gluo.cloud` in your web browser to check if the nginx webpage is visible.

## 2. Azure configuration ##

Before we can deploy our infrastructure into Azure using Terraform we need to make some small changes to the Terraform code.  More specifically we need to edit the `variables.tf` file and specify our group ID.  To do so use either `vi`, `vim` or `nano`.  For example:

```yaml
vim ~/multi-cloud-workshop/lab04_terraform/azure/variables.tf

---
# replace the X with your group ID
variable "groupID" {
  default = "X"   // <========= make change here
}

# ---- DO NOT EDIT anything below this line -----------------------------

variable "lab" {
  default = "4"
}

variable "azure_machine_type" {
  default = "Standard_B1ms"
}

variable "location" {
  default = "westeurope"
}
```

If you have successfully edited and saved the `variables.tf` file you can change into the directory that contains the Terraform code for Azure:

```
cd ~/multi-cloud-workshop/lab04_terraform/azure/
```

Have a look at the content of the Terraform file:

```yaml
cat main.tf

---
provider "azurerm" {
    features {}
}

terraform {
  backend "local" {
  }
}

resource "azurerm_public_ip" "myterraformpublicip" {
    name                         = "terraform-multicloud-ip-group${var.groupID}"
    location                     = var.location
    resource_group_name          = "multicloud-workshop-${var.groupID}"
    allocation_method            = "Static"
}

resource "azurerm_network_security_group" "myterraformnsg" {
    name                = "terraform-multicloud-secgroup-group${var.groupID}"
    location            = var.location
    resource_group_name = "multicloud-workshop-${var.groupID}"
    
    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
    
		security_rule {
        name                       = "HTTP"
        priority                   = 1011
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "80"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
    depends_on = [ azurerm_network_interface.myterraformnic, azurerm_virtual_machine.myterraformvm ]
}

resource "azurerm_network_interface" "myterraformnic" {
    name                = "terraform-multicloud-nic-group${var.groupID}"
    location            = var.location
    resource_group_name = "multicloud-workshop-${var.groupID}"

    ip_configuration {
        name                          = "terraform-multicloud-nicCfg-group${var.groupID}"
        subnet_id                     = "/subscriptions/8af816e4-eed0-4836-8f18-51bed0cd797f/resourceGroups/multicloud-workshop/providers/Microsoft.Network/virtualNetworks/multicloud-workshop-vnet/subnets/default"
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id          = azurerm_public_ip.myterraformpublicip.id
    }
}

resource "azurerm_virtual_machine" "myterraformvm" {
    name                  = "terraform-instance-azure-group${var.groupID}"
    location              = var.location
    resource_group_name   = "multicloud-workshop-${var.groupID}"
    network_interface_ids = [azurerm_network_interface.myterraformnic.id]
    vm_size               = var.azure_machine_type

    storage_os_disk {
        name              = "myOsDisk-group${var.groupID}"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Premium_LRS"
    }
    delete_os_disk_on_termination = true

    storage_image_reference {
        publisher = "canonical"
        offer     = "0001-com-ubuntu-server-focal"
        sku       = "20_04-lts-gen2"
        version   = "latest"
    }

    os_profile {
        computer_name  = "terraform-instance-azure-group${var.groupID}"
        admin_username = "ubuntu"
        custom_data    = templatefile("cloud-init.yaml",{group_id = "${var.groupID}"})
    }

    os_profile_linux_config {
        disable_password_authentication = true
        ssh_keys {
            path     = "/home/ubuntu/.ssh/authorized_keys"
            key_data = file("~/.ssh/workshop_key.pub")
        }
    }

    tags = {
        group = "${var.groupID}"
        lab = "${var.lab}"
    }

    depends_on = [
      azurerm_network_interface.myterraformnic
    ]
}

resource "azurerm_network_interface_security_group_association" "nicassosiation" {
  network_interface_id      = azurerm_network_interface.myterraformnic.id
  network_security_group_id = azurerm_network_security_group.myterraformnsg.id
}

data "azurerm_public_ip" "test" {
  name                = azurerm_public_ip.myterraformpublicip.name
  resource_group_name = "multicloud-workshop-${var.groupID}"
}

resource "azurerm_dns_a_record" "myterraformdns" {
  name                = "${var.groupID}.tf"
  zone_name           = "workshop.azure.gluo.cloud"
  resource_group_name = "rg-gluo-core"
  ttl                 = 60
  records             = [data.azurerm_public_ip.test.ip_address]
}

output "public_ip_address" {
  value = data.azurerm_public_ip.test.ip_address
}
```

Now we need initialize our working directory for Azure using the `terraform init` command again.

```
terraform init
```

Now we can run the `terraform plan` command again to see what Terraform is going to do.

```
terraform plan
```

If you are happy with what Terraform is going to to do you can run the `terraform apply` command again.

```
terraform apply
```

Visit `X.tf.workshop.azure.gluo.cloud` in your web browser to check if the nginx webpage is visible.

## RUN CHECKSCORE SCRIPT ON YOUR MANAGEMENT VM

Run the checkscore command.
- `sudo checkscore 4`

## 4. DO NOT FORGET Cleanup ##
`This is important because in the next lab we will setup the same resources using a pipeline!!`

You should have noticed how easy it is to deploy similar infrastructure to different clouds using the same tools/language. It is equally easy to clean up (destroy) infrastructure resources you created with Terraform. With the following commands you will remove all the resources you previously created.

Make sure that everything is deleted correctly, if you run into any issues let one of the instructors know.

Delete all resources on AWS. When prompted for a value, answer with "yes":

```
cd ~/multi-cloud-workshop/lab04_terraform/aws/

terraform destroy
``` 

Delete all resources on Azure. When prompted for a value, answer with "yes":

```
cd ~/multi-cloud-workshop/lab04_terraform/azure/

terraform destroy
```

## 5. Conclusion ##

In this lab you created infrastructure on 3 different cloud providers by using the Infrastructure-as-Code tool Terraform. You have witnessed how easy it is to deploy and destroy infrastructure in just a matter of time. 

---

Go to the next lab: [Lab 05 - Infrastructure pipeline](../lab05_infra_pipeline)